* * *

CYBESYNC  - Cyberlearn Download Utility
=======================================

DESCRIPTION
------------
Always wanted to keep in sync with a course from Cyberlearn but too lazy to find out which
pdf/resource you already downloaded ? Tired of login every two minutes because of the silly
timeouts of the plateform ?  This program is made for you !

Just initialize cybe in a directory and use one command to download all the new resources
at once.

SYNOPSIS
---------

`cybe  [ init | init-global | pull | add-origin ]`

   
     

Commands         | Description
:----------------|:--------------------------------------------------------------------------
 `init`          | bind the current directory to one of your Cyberlearn course
 `init-global`   | store your credentials once and for all
 `add-origin`    | add an url to the parsing process
 `pull`          | download the latest resources for the course bound to the local directory
 `oneshot <url>` | parse the given url and download resources in the current directory
                 |
                 |
**Help**         |
 `help`          | print a brief help message
 `man`           | display the full documentation


CYBE COMMANDS
-------------
In each of your course directory, use `cybe init` to specify to which course it belongs and
what are your credentials.

You can then use `cybe pull` to download the resources of this course. Each time `cybe pull` is
called, it will first check in the current directory to find out which resources you
already have, to avoid unnecessary downloads.

If you do not want to give your credentials everytime you init a local directory, use `cybe
init-global`. It will save them somewhere in your home directory and you are done.

If some pages related to a course are not found, you can manually add them with the command
`cybe add-origin <url>`. The next time you pull, the url given will also be parsed in search
for resource links.

If you just want to download the resources from a link, but don't need sync, use `cybe
oneshot <url>`. It will parse the given page and download resources in the current directory
and exit. Note: you need at least a global configuration to use it (for username and  password).

REQUIRES
--------

You will need perl installed on your machine, as well as some perl packages. Here is a list:

+ HTML::TokeParser::Simple
+ LWP::UserAgent
+ File::Spec::Functions
+ HTTP::Request
+ HTTP::Cookies
+ HTML::Parser
+ Cwd
+ JSON
+ Pod::Usage
- Carp
- Data::Dumper


KNOWN ISSUES
------------

###Linux readline
If you have troubles with the prompt, check that you have the PERL\_RL variable set and the libperl-gnu-readline-support installed. You can add those lines to your .bashrc: 

`export "PERL_RL= o=0"`

###CyberlearnConnector not found
If you eant to move cybe on another directory, beware of moving CyberlearnConnector.pm along with it.

* * *

SIMPLECRAWLER
=============

This is a simple project which achieves two goals: checking dead links on a given domain and/or generating a sitemap.

SimpleCrawler.pm is a generic module offering the crawling functionality (runs multiple threads). LinkUtils.pl contains the main code, offering both checking and sitemaping (depending on the command-line options).

Right now, it is more like a test than a real program; so just ignore it.
