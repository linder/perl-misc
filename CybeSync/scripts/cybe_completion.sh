#!/bin/bash

commands=(init pinit pull init-global 
          add-origin remove-origin add-ctype remove-ctype 
          help man
         )

_cybe () {   
  local cur
  # Pointer to current completion word.
  # By convention, it's named "cur" but this isn't strictly necessary.


  COMPREPLY=()   # Array variable storing the possible completions.
  cur=${COMP_WORDS[COMP_CWORD]}


  if [[ "${COMP_WORDS[*]}" =~ remove-(origin|ctype) ]]; then
     key=$( echo "${COMP_WORDS[*]}" | sed 's/.*remove-\([^ ]*\).*/\1/')
     test -n "$key" || return 1 

     COMPREPLY=( $( compgen -W "$( perl -e '
                use JSON; open( F, ".cybe"); $/ = undef; 
                $h = JSON::from_json(<F>);  close F; 
                for ( @{$h->{'$key'}} ){ print "$_\n"; }
         ' 2>/dev/null )" -- "$cur" ) )
   else
        COMPREPLY=( $(compgen -W "${commands[*]}" -- $cur) ) 
   fi
     
  return 0

}

complete -F _cybe -o filenames cybe
