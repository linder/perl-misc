#!/usr/bin/env perl

use warnings;
use strict;
use utf8;
use Pod::Autopod;

my $path = shift;
my $out = shift;

if( not $path ){
    print "Usage: autopod source_file [outfile]\n";
    exit(1);
}

if( not $out ){
    $out = "out.pod";
}

my $pod = new Pod::Autopod( readfile => $path );

open F, ">", $out or die( "Could not open file $out for writing" );
print F $pod->getPod();
close F;

print "done\n";

