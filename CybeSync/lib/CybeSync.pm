#!/usr/bin/env perl

package CybeSync;

#use warnings;
use strict;
use utf8;

use Data::Dumper;
use Carp;
use Encode qw( encode decode );
#use Pod::Usage;

use LWP::UserAgent;
use LWP::Protocol::https;
use HTTP::Request;
use HTTP::Cookies;
use URI::Escape;


use Term::ReadKey;
use Term::ReadLine;

my $WINDOWS = ( $^O eq "MSWin32" );
our $VERSION = '1.01';

# push @INC, ( Cwd::abs_path($0) =~ /(.*\/)[^\/]*/ and $1 );
# require CyberlearnConnector;

use Exporter 'import';
our @EXPORT_OK = qw( find_resources download_resources write_to_file );

my $debug = 1;

my @ACCEPT_CONTENT_TYPES = qw( plain msword zip pdf excel powerpoint officedocument forcedownload ); # the content-types to download 

my $ACCEPT_CT_REGEX = "(" . join( "|", @ACCEPT_CONTENT_TYPES ) . ")"; # as a regex 

my $filter = sub { # default filter for resources
    my ($url, $name, $ctype) = @_;
    $ctype =~ /$ACCEPT_CT_REGEX/; 
};

# use Pod::Autopod;
# new Pod::Autopod(readfile=>'Foo.pm', writefile=>'Foo2.pm');
# my $ap = new Pod::Autopod(readfile=>'CybeSync.pm');
# print $ap->getPod();

############################################ courses and resource 

sub find_resources {
    my ( $ua, $urls, $customfilter ) = @_;

    croak( "missing argument. Usage : \$ua, \$url\n" )
        and return '' unless $ua and scalar (@$urls) > 0;

    # ---------- init
    $filter = $customfilter if $customfilter and ref $customfilter eq 'CODE';
    my %new_resources; # where to store the resources found
    my $course = shift @$urls;
    my $resp = $ua->get( $course ); # get the page

    croak( "Could not download page content ( " . $resp->status_line . "\n" )
    unless $resp->status_line =~ /200/;

    # ---------- parse the "base" page
    my %candidates; # pages potentially containing resources 

    print "Beginning parsing...\n";
    @_ = $resp->content =~ /(https?:\/\/[^\"<>]*(\.pdf|resource)[^\"<>]*)/g;
    map { $candidates{ $_ } = 1 if $_ =~ /^http/ } (@_, @$urls);

    # ---------- iterate through the potential resources (open each page)
    foreach my $candidate ( keys %candidates ){ # parse all the potential pages

        if( $candidate =~ /\.pdf$/ ){ # it is [normally] a pdf
            $_ = decode( "utf8", uri_unescape( $1 ) ) 
                if $candidate =~ m/([^\/]+pdf)/; 

            if( $filter->( $candidate, $_, "application/pdf" ) ){
                print "passed filter: $_\n" if $debug;
                $new_resources{ $_ } =  $candidate;
            }

        }else{ # cannot infer the content-type based on the link
            my $resp = $ua->head( $candidate );
            next unless $resp->status_line =~ /200/;

            my $name = encode_decode( uri_unescape( $resp->filename ) ); 
            my $ctype = $resp->header( 'content-type' );


            if( $ctype =~ m|text/html| ){ # not a resource, 
                $resp = $ua->get( $candidate );
                # but containing a link to one ?
                @_ = $resp->content =~ m/http:\/\/[^\"]+\.pdf/g; # get all the pdf links

                #TODO : and if not a pdf but a useful resource ?
                foreach my $url ( $resp->content =~ /http:\/\/[^\"]+\.pdf/g ){
                    $_ = encode_decode( uri_unescape( $1 ) )
                         if $url =~ m/([^\/]+pdf)/; 

                    if( $filter->($url, $_, "application/pdf") ){
                        $new_resources{ $_ } = $url; 
                        print "passed filter: $_\n" if $debug;
                    }
                }

            }elsif( $filter->( $candidate, $name, $ctype ) ){
                print "passed filter: $name\n" if $debug;
                $new_resources{ $name } =  $candidate;
            }
        }
    }

    print "\n  --- FOUND ", scalar( keys %new_resources ), " new resources for '$course'\n\n";
    print Dumper( \%new_resources ) if $debug;

    return \%new_resources;
}

#------------------------------------------ 

##
# download all the resources from the given urls in the current directory
# arguments:
# - ua : the UserAgent to use (must be connected to cyberlearn)
# - resources : a hash reference to the resources. key = name, value = url  
sub download_resources { # void ( \$useragent, \$resources_hash_ref )
    my ( $ua, $resources ) = @_;
    #$resources = $args->{ resources } or '' unless $resources;
    croak( "Missing useragent. Can do nothing !\n" ) unless $ua;

    my %inodes;

    foreach my $key ( keys %$resources ){
        $_ = download_res( $ua, $key, $resources->{$key} );
        $inodes{ $_ } = $key if $_;
    }

    return \%inodes;
}

sub download_res { # void ( \$useragent, \$resources_hash_ref )
    my ( $ua, $name, $link ) = @_;
    #$resources = $args->{ resources } or '' unless $resources;
    croak( "Missing useragent. Can do nothing !\n" ) unless $ua;

    return '' unless $link =~ m|^https?://|;
    my $resp = $ua->get( $link );

    if( $resp->status_line !~ /200/ ){
        print "Could not download resource $name : ", $resp->status_line, "\n";
        return '';
    }

    print "Could not write $name\n" and return '' 
        unless write_to_file( $name, $resp->content );

    print " ** Downloaded $name\n";
    return $name;
}

#------------------------------------------  

sub encode_decode{
    my $string = shift;
    $_ = ( $WINDOWS ) ? 
        encode("cp1252", decode( "utf8", $string ) ) : $string;
    return $_;
    
}
##
# write some content into a file
sub write_to_file { # \$boolean_ok ( \$filename, \$content )
    my ( $name, $content ) = @_;

    local *F;
    open F, ">:raw", $name or print $! and return '';
    print F $content;
    close F;
    return 1;
}

1;

