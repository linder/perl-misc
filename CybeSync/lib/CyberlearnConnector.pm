#!/usr/bin/env perl

package CyberlearnConnector;

#~ use warnings;
use strict;
use utf8;

use Data::Dumper;
use Carp;

use LWP::UserAgent;
use HTTP::Request;
use HTTP::Cookies;
use HTML::TokeParser::Simple;
use HTML::Parser;

our $VERSION = '1.00';

my $debug = 0;

############################################ new

sub new {
    my ( $class, $auth, $verbose ) = @_;

    my ( $useragent, $welcome_page ) = __authenticate( $auth, $verbose);
    print "Authentication succeeded\nFinding your courses\n" if $verbose;

    my $self = {
        cookies     => $useragent->cookie_jar,
        ua          => $useragent,
        courses     => __get_courses( $welcome_page, $verbose ),
        connected   => 1
    };

    bless $self, $class;
    return $self;
}

############################################ getters / setters


sub courses { # @course_names ( \$self )
    my ( $self ) = @_;
    return keys %{ $self->{ courses } };
}

sub course_url{ # \$course_url ( \self, \$course_name [, \$course_url ] )
    my ( $self, $course, $url ) = @_;
    defined $url ? $self->{ courses }->{ "$course" } = $url : $self->{ courses }->{ "$course" }; 
}

sub ua { # LWP::UserAgent ( \$self ) # read-only
    shift->{ ua };
}

##
# return a newe UserAgent connected to Cyberlearn
sub ua_clone { # LWP::UserAgent ( \$self ) 
    my ( $self ) = @_;
    return LWP::UserAgent->new( cookie_jar => $self->cookies );
}

##
# return the cookies used to connect to cyberlearn (beware of timeouts)
sub cookies { # HTTP::Cookies ( \$self )
    return shift->{ cookies };
}


############################################ connect and disconnect

sub __authenticate { # LWP::UserAgent ( \%auth ) auth contains the key username and password
    my ( $auth, $verbose ) = @_;
    # validate the parameters
    croak( "Usage : CyberlearnConnector->new( { username => ..., password => ... }, \$verbose )" ) 
        unless defined $auth and  exists $auth->{ username } and exists $auth->{ password };

    #~ my $cookie_jar = HTTP::Cookies->new(
    #~ file => 'lwp_cookies.txt',
    #~ autosave => 1,
    #~ ignore_discard => 1,
    #~ );
    #~ my $ua = LWP::UserAgent->new( cookie_jar => $cookie_jar );

    my $ua = LWP::UserAgent->new( cookie_jar => HTTP::Cookies->new() );
    push @{ $ua->requests_redirectable }, 'POST';

    # fill the organisation form
    print " * filling organisation form\n" if $verbose;
    my $resp = $ua->post( 'https://wayf.switch.ch/SWITCHaai/WAYF?entityID=https%3A%2F%2Fcyberlearn.hes-so.ch%2Fshibboleth&return=https%3A%2F%2Fcyberlearn.hes-so.ch%2FShibboleth.sso%2FLogin%3FSAMLDS%3D1%26target%3Dhttps%253A%252F%252Fcyberlearn.hes-so.ch%252Fauth%252Fshibboleth%252Findex.php',
        Content => {'user_idp' => 'https://aai-logon.hes-so.ch/idp/shibboleth'} );
    print $resp->status_line, "\n" if $debug;
    die "Could not connect (login form)\n" unless $resp->status_line =~ /200/;

    my %post_args;
    $post_args{ j_username } = $auth->{ username };
    $post_args{ j_password } = $auth->{ password };

    # get the SAML input fields which must be sent with the credentials
    my $parser = HTML::Parser->new();
    $parser->report_tags( qw( input ) );
    $parser->handler( start => sub{
        my ( $self, $tagname, $attr, $attrseq, $text ) = @_;
            if( $attr->{ type } and $attr->{ type } eq 'hidden' and $attr->{ name } ){
                $post_args{ $attr->{ name } } = $attr->{ value };
            }
        }
    );
    print " * filling the authentication form\n" if $verbose;
    $parser->parse( $resp->content );

    # get the username-password form
    $resp = $ua->post( 'https://aai-logon.hes-so.ch/idp/Authn/UserPassword', 
        Content => \%post_args );
    die "Could not connect(post credentials) \n" unless $resp->status_line =~ /200/;

    print " * handling the response\n" if $verbose;
    $parser->parse( $resp->content ); # get the SAML hidden fields a second time, just in case...
    
    print " * getting the welcome page\n" if $verbose;
    
    # post the user-pass form, which redirects to the welcome page
    $resp = $ua->post( 'https://cyberlearn.hes-so.ch/Shibboleth.sso/SAML2/POST', 
        Content => \%post_args );
    print $resp->status_line, "\n" if $debug;
    die "Could not connect (post welcome page)\n" unless $resp->status_line =~ /200/;

    return ( $ua, $resp->content );
}


sub disconnect{ # void ( $LWP_UserAgent )
    my ($self, $ua ) = @_;
    return unless $self->is_connected();

    $ua = $self->ua if ref $self;
    $ua->get('http://cyberlearn.hes-so.ch/login/logout.php');
    $self->{ connected } = '';

    print "Disconnected from Cyberlearn\n";
}


############################################ courses 

##
# return a reference to a hash containing the "my courses" found in
# the welcome page as keys and their url as value.
sub __get_courses { # \$courses_hash_ref ( $welcome_page_content )
    my ( $content ) = @_;
    my %courses;
    my $p = HTML::TokeParser::Simple->new( \$content );

    while ( my $div = $p->get_tag("li") ) {
        next unless $div->get_attr( 'class' ) 
            and $div->get_attr( 'class' ) =~ 'type_course';

        my $a = $p->get_tag('a');
        my $title = $a->get_attr('title');
        next unless $title;

        $courses{ $title } = $a->get_attr('href');
    }

    print Dumper(\%courses) if $debug;
    return \%courses;
}

sub is_connected{
    my $self = shift;
    return $self->{ connected };
}
1;
