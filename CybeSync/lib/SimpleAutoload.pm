#!/usr/bin/env perl

use strict;
use utf8;

use Data::Dumper;
use JSON;

package main;

#testing();

sub testing {
    my $test = SimpleAutoload->new();
    $test->load_from( "/home/lucy/school/Microprocesseurs/pdfs/.cybe" );
    print Dumper( $test );

    print $test->course, "\n";
    $test->course( "new value" );
    print $test->course, "\n";
    print $test->add_origin( "a new origin" ), "\n";
    print $test->add_origin( "another origin" ), "\n";
    print $test->origin, "\n";
    $test->remove_origin( "another origin" ), "\n";
    $test->remove_origin( "a new origin" ), "\n";
    print "removed ", $test->origin, "\n";
    print "\nnbr of modifs ", $test->__modified, "\n";
    $test->reset_modified();
    print "\nnbr of modifs ", $test->__modified, "\n";
}

package SimpleAutoload;

use Data::Dumper;
use vars '$AUTOLOAD';

sub new {
    my $class = shift;
    my $self = { '__private__modified_' => 0 };
    bless $self, $class;
    return $self;
}

sub load_from { # \$local_config_hash_ref ( void )
   my ( $self, $path ) = @_;

   if( -e $path ) {
        local( *F );
        open( F, "$path" ) or 
            die "Could not open .cyberlearn file. Have you tried ceby init ?\n";
        local $/ = undef;
        my $h = JSON::from_json( <F> );
        map { $self->{$_} = $h->{$_} } keys %$h;
        close F;
    } 

    $self->__reset_modified();
    return $self;
}

 
 sub AUTOLOAD {
    my ( $self, $value ) = @_;
    return if ( $AUTOLOAD =~ /DESTROY/ );   # don't mess with garbage collection
    ( my $key = $AUTOLOAD ) =~ s/.*:://;

    #print "AUTOLOAD: ", $key, "\n";

    if( $key =~ /^add_(.*)/g ){

        $key = $1;
        return unless $value;
        
        # the array does not exist, create it
        if( not $self->__key_exists( $key ) ){
            $self->{ $key } = [ $value ]; 
            $self->{ '__private__modified_' }++;
            return;
        } 

        # return if not an array
        return '' unless $self->__is_array( $key );
 
        # else, add value to it
        push @{ $self->{ $key } }, $value;
        $self->{ '__private__modified_' }++;
        
        return $self->__get( $key );
        
    }elsif( $key =~ /^remove_(.*)/g ) {
       
        $key = $1;
        # be sure that the array exists
        return unless ( $value && $self->__is_array( $key) );
        # remove the matching element
        
        @{ $self->{$key} } = grep{ 
            if( $_ eq "$value" ){ 
                $self->{ '__private__modified_' }++; 
                0; 
            }else{ 1; } 

        } $self->__get( $key );

        # return the array
        return $self->__get( $key );
    
    }else{
        # getter of setter
        $self->{ $key } = $value and $self->{ '__private__modified_' }++ 
            if defined $value;

        return $self->__key_exists( $key ) ? $self->__get( $key ) : undef;

    }
 }
 
 sub __get_hash {
    my ( $self ) = @_;
    my %hash;
    map { if( $_ !~ /^__/ ){ $hash{ $_ } = $self->{ $_ }; } } keys %$self;
    return %hash;
 }
 
 sub __is_array {
    my ( $self, $key ) = @_;
    $self->{ $key } =~ /^ARRAY/;
 }
 
 sub __get {
    my ( $self, $key ) = @_;
     return $self->__is_array( $key ) ?  
        @{ $self->{ $key } } : 
        $self->{ $key };
 }
 
 sub __set {
    return unless scalar @_ == 3;

    my ( $self, $key, $value ) = @_;
    $self->{ '__private__modified_' }++; 
    $self->{ $key } = $value;
 }

 sub __unset {
    my ( $self, $key ) = @_;
    $self->{ '__private__modified_' }++; 
    delete $self->{ $key };
 }


 sub __reset_modified {
    my ( $self ) = @_;
    $self->{ '__private__modified_' } = 0; 
 }

 sub __modified {
     my ( $self, $val ) = @_;
     defined $val ? $self->{ '__private__modified_' } = $val : 
        $self->{ '__private__modified_' }; 
     
 }

 sub __key_exists {
    my ( $self, $key ) = @_;
    exists $self->{ $key };
 }
1;
