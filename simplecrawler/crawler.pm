#!/usr/bin/env perl

package Sitemap::Gen;

use warnings;
use strict;
use LWP::Simple qw($ua get head);
$ua->timeout(30);
use Data::Dumper;
use threads;
use Thread::Queue;
use threads::shared;
use DateTime;
use JSON;
use HTML::Parser;
use Getopt::Long;

my $COUNT = 0; my $CONTENT_TYPE = 1; my $LAST_MOD = 2;    
my $queue : shared;
my ( %parsed, %found, @counts, @content_types, @last_mod, $running, @active ) : shared; # to keep track of parsed pages

# ---------------- main --------------

#my $base_link = "http://www.perrin-traiteur.ch";
#my $base_link = "http://www.santeparlayurveda.ch";
my $base_link = "http://www.madeleine-traiteur.ch";
#my $base_link = "http://dytherapie.ch";
gather_links( $base_link );


# ---------------- gathering the links --------------

# worker thread : job definition
sub thread_job{ # @links_found ( \%visited_pages_so_far, $base_link )
    my ( $id ) = @_; 
    # to store the links found in all pages parsed by this thread
    
    my $parser = LinkParser->new(  );
    $parser->handler( start => 
        sub{
            my ( $self, $tagname, $attr, $attrseq, $origtext ) = @_;
            
            if ($tagname eq 'a' and $attr->{ href } and not $attr->{ href } =~ /^((#)|(mailto)).*/ ) {
                my $url = $self->resolve( $attr->{ href } );
                return if not $url or not $url =~ /^$base_link/ or $url =~ /^(#|.+#[^\/]*)/;
                
                if( exists $found{ $url } ) { 
                    $found{ $url } =~ s/^([0-9]+)/int($1) + 1/e; 
                    return; 
                };

                $found{ $url } = "1";
                $queue->enqueue( $url ); #unless $_ =~ /\.((jpg)|(png)|(css)|(js)|(pdf)|(xml)|(gif))$/i;
            } 
        } 
    );
    $parser->report_tags( qw( a ) );
    
    my $counter = 0;
    # while there are pending jobs in the queue
    while ( $running  ) {
        my $item = $queue->dequeue_nb();
        if( not $item ){
            print "  $id : waiting...\n";
            $active[$id] = 0;
            sleep 4;
            next;
        }

        $active[$id] = 1;
        $parsed{ $item } = 1; # marks the page as visited
        
        # continue if the page has already been visited (or is null)
        next if not $item or $parsed{ $item };
        
        # get header infos "last mod" just in case
        my @headers = head( $item );
        next if not @headers;
        
        $found{ $item } = "1" if not $found{ $item };
        $found{ $item } .=  "#" . ( $headers[0] =~ /^([^;]+)/ and $1 ) . "#";
        if( $headers[2] ){
            $_ = DateTime->from_epoch( epoch => $headers[2] )->ymd( '-' );
            /(^[0-9]{4}-[0-9]{2}-[0-9]{2}).*/;
            $found{ $item } .=  $1;
        }
        
        my $is_html = $headers[0] =~ /text\/html/;
        # don't parse the page if it has an extension
        next unless $is_html; #/\.((jpg)|(png)|(css)|(js)|(pdf)|(xml)|(gif))$/i;
        
        # creates and launches the parser
        print "$id parsing : $item\n";
        $parser->{ base_url } = $item;
        $parser->parse( get( $item ) );
        print "Error : $@\n" and next if $@;
        print $id, " queue: ", $queue->pending(), "\n";
        #$counter = 0 and sleep 1 if $counter > 3;
    }
    
    #return \@links_found;
}






# crawls the website and returns a hash where the keys are the urls (from the same domain)
# found and the values the number of times the url was encountered 
sub gather_links{ # %links ( $base_url )

    #my ($base_link ) = @_;
    
    # initialises the job queue
    $queue = Thread::Queue->new();
    $queue->enqueue( $base_link );
    
    # initialises the variables
    %parsed = (); %found = (); @active = ();
    $running = 1;
    my @threads;              # threads


    # launches the treads
    for (0 ... 3){
        $active[$_] = 1;
        push @threads, threads->create( \&thread_job, $_, \%parsed, \%found, \$running, \@active, $base_link );
    }
       
    while( grep /1/, @active ){
        sleep 3;
    }
    
    $running = 0;
    
    # gather the results
    for( @threads ){
        my $r = $_->join();
    }

    print Dumper( \%parsed );
    print Dumper( \%found );
    print "parsing done...\n";
}  


# removes all the duplicates from an array
sub distinct{ # \@ ( \@ )
    # the idea is to convert the array into a hash, since hash keys must be
    # unique, and then get the keys back
    my %h;
    return grep { !$h{$_}++ } @_
}



# ---------------------- parser ---------------------------------
package LinkParser;

# simple parser, which : 
# finds all the <a> tags in a page, resolves their href using the base url provided
# and gathers the latter in self{ links }
# http://perlmeme.org/tutorials/html_parser.html
# http://search.cpan.org/dist/HTML-Parser/Parser.pm
use warnings;
use strict;
use URI;
use LWP::Simple;
use HTML::Parser;
use threads;
use Data::Dumper;
use threads::shared;
use Carp;
use base qw(HTML::Parser);


#~ sub new{
    #~ my ( $class, $link ) = @_;
    #~ my $self = $class->SUPER::new();
    #~ $self->{ base_url } = $link; 
    #~ print "----- new parser created : ", $self->{ base_url }, "\n";
    #~ return $self;
#~ }

# parses the page pointed by the given url and returns all the links found
# the parsing is recursive (in the same domain )
sub get_and_parse{ # @links_found ( $self, $base_url )
    my ( $self, $link ) = @_;
    croak( "no link provided" ) if not $link;
    
    eval{ $self->parse( get( $link ), $link ); };
    croak( "Oops, error while getting and parsing: $@" ) if $@;
    
    #return @{ $self->{ links } };
}

# parses the content of the page and finds all the links
# the parsing is recursive, hence the need to provide the source url (for relative links resolution)
#~ sub parse{ # \@links_found ( $self, $content, $source_url )
    #~ my ( $self, $content, $source_url ) = @_;
    #~ croak( "content is required" ) unless $content;
    #~ # re-init the vars
    #~ $self->{ base_url } = $source_url unless not $source_url;
    #~ $self->{ links } = ();
    #~ # parses
    #~ $self->SUPER::parse( $content );
    #~ #return @{ $self->{ links } };
#~ }


# returns the absolute url, using the url given in get_and_parse as a base
sub resolve{ # $absolute_url ( $self, $url )
    my ( $self, $url ) = @_;
    return if not $url or not $self->{ base_url };
    $_ = URI->new_abs( $url, $self->{ base_url } )->as_string(); 
    $url = $1 if( $_ =~ /(.*)\/?$/ );
    return $url;  
}


# arguments for the handlers : 
#   $tagname    => the name of the start tag, 
#   $attr       => hash-reference containing the attributes as key/value pairs, 
#   $attrseq    => an array-reference which lists the attribute keys in the order in which they appeared in the tag
#   $origtext   => the original text as it appeared in the HTML snippet
sub start{ 
    my ( $self, $tagname, $attr, $attrseq, $origtext ) = @_;
    
    if ($tagname eq 'a' and $attr->{ href } and not $attr->{ href } =~ /^((#)|(mailto)).*/ ) {
        my $url = $self->resolve( $attr->{ href } );
        #print "     URL found: $url\n";
        push @{ $self->{ links } }, $url
    }
}

