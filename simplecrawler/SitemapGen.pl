#!/usr/bin/env perl

package Sitemap::Gen;

use warnings;
use strict;
use LWP::Simple;
use Data::Dumper;
use threads;
use Thread::Queue;
use threads::shared;
use DateTime;
use JSON;
use Getopt::Long;
my $queue : shared;


# ---------------- main --------------

my $base_link = "http://www.santeparlayurveda.ch/index-glossaire";
#my $base_link = "http://dytherapie.ch";

my $options = {
    last_mod => '',
    no_img => '',
    excluded => '', 
    excluded_extensions => [ 'jpg', 'png' ],
};

GetOptions(

    'excluded=s' => sub{
        my ( $opt_name, $opt_value ) = @_; 
        @_ = split( ',', $opt_value );
        $options->{ excluded } = \@_ unless not scalar( @_ ); 
    },
    
    'noimg' => sub{ $options->{ no_img } = 1; },
    
    'lastmod=s' => sub{ 
        my ( $opt_name, $opt_value ) = @_; 
        print "Error: lastmod options takes a date in the following format : YYYY-MM-dd\n" and exit()
            if $opt_value !~ /^20[0-9]{2}-((0[0-9])|(1[0-2]))-(([0-2][0-9])|(3[0-1]))$/;
        
        $options->{ last_mod } = $opt_value; 
    }
);

#~ print Dumper( $options );
#~ exit();

#print 
gather_links( $base_link );
print "finished
";
exit();

#~ my $rlinks = gather_links( $base_link );
#~ 
#~ foreach ( sort keys %$rlinks ){
    #~ print $_, "  encountered ", $rlinks->{ $_ }, " times\n";
#~ }

# loads for debug
#~ open F, "./links.tmp";
#~ my $str = '';
#~ while(<F>){ $str .= $_ };
#~ my $links = from_json( $str );
#~ close F;
#~ $str = '';
#~ open G, "./visited.tmp";
#~ while(<G>){ $str .= $_ };
#~ my $visited = from_json( $str );
#~ close G;
#~ print create_sitemap( $links, $visited );



#~ 
#~ my %sorted; 
#~ foreach( sort { $visited->{ $a } <=> $visited->{ $b } } keys %$visited ){
    #~ print $_;
#~ }
#~ 
#~ foreach( keys %$links ){
    #~ push @{ $sorted{ $links->{ $_ } } }, $_; 
#~ }
#~ 
#~ my $wheight;
#~ foreach( keys %sorted ){ $wheight += $_ };
#~ my $percent = 100 / $wheight;
#~ 
#~ 
#~ while ( my ($key, $value) = each %sorted ){
    #~ print " $key =>", scalar ( @$value ), "\n";
    #~ #$sorted{ $key } = $percent * $key;
    #~ $sorted{ $key } = $percent * $key;
#~ }
#~ 
#~ 
#~ print Dumper( \%sorted );
#~ print $percent;








# ---------------- creates the sitemap --------------

sub create_sitemap{
    
    my ( $rlinks, $rvisited ) = @_;
    print "entering create sitemap.... \n";
    my $sitemap = '
<?xml version="1.0" encoding="UTF-8"?>
<urlset
      xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9
            http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">';
    
    #~ my @links;
    #~ 
    #~ my ( $reg2 );
    #~ #$reg1 = '^(' . join( ')|(', $options->{ excluded } ) . ')$' if $options->{ excluded };
    #~ 
    #~ print "reg 2 : $reg2  \n";
    #~ if( $reg2 ){
        #~ #@links = grep { not ( ( $reg1 and /$reg1/ ) or ( $reg2 and /$reg2/ ) ) } keys %$rlinks;
        #~ @links = grep { not /$reg2/ } keys %$rlinks;
    #~ }else{
        #~ @links = sort keys %$rlinks ;    
    #~ }
    
    #~ if( $options->{ excluded } ){
        #~ map { delete $rlinks->{ $_ } if exists $rlinks->{ $_ } } @{ $options->{ excluded } };
    #~ }

    my $regex = '\.(' . join( ')|(', $options->{ excluded_extensions }  ) . ')$' if $options->{ excluded_extensions };
    my $default_last_mod = DateTime->now(time_zone=>'local')->ymd('-');
    
    foreach my $link ( sort keys %$rlinks ){
        
        next if defined $regex and $link =~ /$regex/i;
        next if $options->{ excluded } and grep { $link } @{ $options->{ excluded } }; 
        
        $sitemap .= "
        <url>
          <loc>" . $link . "</loc>
          <lastmod>"; 
          if( $options->{ last_mod } ){
            $sitemap .= $options->{ last_mod };
          }else{
                $sitemap .= ( $rvisited->{ $link } eq '1' ? $default_last_mod : $rvisited->{ $link } );
          }
          
        $sitemap .= "</lastmod>
        </url>
        ";
    }
    
    $sitemap .= '</urlset>
    '; 
    return $sitemap;
}


# ---------------- gathering the links --------------

# worker thread : job definition
sub thread_job{ # @links_found ( \%visited_pages_so_far, $base_link )
    my ( $visited, $base ) = @_; 
    # to store the links found in all pages parsed by this thread
    my @links_found = ();
    my $parser = LinkParser->new( );
    
    # while there are pending jobs in the queue
    while ( $queue->pending() > 0  ) {
        
        my $item = $queue->dequeue_nb();
        print "*** $item \n";
        # continue if the page has already been visited (or is null)
        next if not $item or $visited->{ $item };
        # get header infos "last mod" just in case
        @_ = head( $item );
        next if not @_;
        my $is_html = $_[0] =~ /text\/html/;
        
        if( $_[2] ){
                $_ = DateTime->from_epoch( epoch => $_[2] )->ymd( '-' );
                /(^[0-9]{4}-[0-9]{2}-[0-9]{2}).*/;
                $visited->{ $item } = $1;
        }else{
            $visited->{ $item } = 1; # marks the page as visited and store last modified if any
        }
        # don't parse the page if it has an extension
        next unless $is_html; #/\.((jpg)|(png)|(css)|(js)|(pdf)|(xml)|(gif))$/i;
        
        # creates and launches the parser
        print "parsing : $item\n";
        my @temp_links;
        eval { @temp_links = $parser->get_and_parse( $item ) };
        print "Error : $@\n" and next if $@;
        # gets the links found from the parser and 
        # filters out all the links from another domain or pointing to an anchor
        @temp_links = grep { /^$base/ and not /^(#|.+#[^\/]*)/ } @temp_links;
        # adds the links found to the global list of links
        @links_found = ( @links_found, @temp_links );
    }
    
    return \@links_found;
}

# crawls the website and returns a hash where the keys are the urls (from the same domain)
# found and the values the number of times the url was encountered 
sub gather_links{ # %links ( $base_url )
    my ($base_link ) = @_;
    
    # initialises the job queue
    $queue = Thread::Queue->new();
    $queue->enqueue( $base_link );
    
    # initialises the variables
    my ( %visited ) : shared; # to keep track of parsed pages
    my @threads;              # threads
    my ($loopcount, %links ); # the links found

    while (1){
        print "\nloop ", $loopcount++ ,"\n";
        last unless $queue->pending(); # exits if queue empty
        # determines how many threads are needed
        @threads = ();
        my $count = $queue->pending();
        $count = 5 unless $count < 10;
        print "count $count\n";
        
        # launches the treads
        for (0 ... $count){
            push @threads, threads->create( \&thread_job, \%visited, $base_link );
        }
        
        my @results;
        # gather the results
        for( @threads ){
            my $r = $_->join();
            @results = ( @results, @$r ); # merges the two arrays
        }
        # updates the links var (and the "encountered count" + filtered out the urls already parsed
        @results = grep { ++$links{ $_ } and not $visited{ $_ } } @results;
        # filters out the .jpg, .pdf etc. 
        @results = distinct( @results ); # grep { not /\.[a-z]{1,4}$/i } distinct( @results );
        # adds the remaining links to the queue before looping again
        $queue->enqueue( @results );
        print "in queue : ", $queue->pending(), "\n";
    }
    
    print "parsing done...\n";
    open F, ">./links.tmp";
    print F to_json( \%links );
    close F;
    open G, ">./visited.tmp";
    print G to_json( \%visited );
    close G;
    #exit();
    #~ #return \%links;
    
    return create_sitemap( \%links, \%visited );
}  


# removes all the duplicates from an array
sub distinct{ # \@ ( \@ )
    # the idea is to convert the array into a hash, since hash keys must be
    # unique, and then get the keys back
    my %h;
    return grep { !$h{$_}++ } @_
}



# ---------------------- parser ---------------------------------
package LinkParser;

# simple parser, which : 
# finds all the <a> tags in a page, resolves their href using the base url provided
# and gathers the latter in self{ links }
# http://perlmeme.org/tutorials/html_parser.html
# http://search.cpan.org/dist/HTML-Parser/Parser.pm
use warnings;
use strict;
use URI;
use LWP::Simple;
use HTML::Parser;
use threads;
use Data::Dumper;
use threads::shared;
use Carp;
use base qw(HTML::Parser);


#~ sub new{
    #~ my ( $class, $link ) = @_;
    #~ my $self = $class->SUPER::new();
    #~ $self->{ base_url } = $link; 
    #~ print "----- new parser created : ", $self->{ base_url }, "\n";
    #~ return $self;
#~ }

# parses the page pointed by the given url and returns all the links found
# the parsing is recursive (in the same domain )
sub get_and_parse{ # @links_found ( $self, $base_url )
    my ( $self, $link ) = @_;
    croak( "no link provided" ) if not $link;
    $_ = get( $link );
    return () unless $_;
    eval{ $self->parse( $_, $link ); };
    croak( "Oops, error while getting and parsing: $@" ) if $@;
    
    return @{ $self->{ links } };
}

# parses the content of the page and finds all the links
# the parsing is recursive, hence the need to provide the source url (for relative links resolution)
sub parse{ # \@links_found ( $self, $content, $source_url )
    my ( $self, $content, $source_url ) = @_;
    croak( "content is required" ) unless $content;
    # re-init the vars
    $self->{ base_url } = $source_url unless not $source_url;
    $self->{ links } = ();
    # parses
    $self->SUPER::parse( $content );
    return @{ $self->{ links } };
}


# returns the absolute url, using the url given in get_and_parse as a base
sub resolve{ # $absolute_url ( $self, $url )
    my ( $self, $url ) = @_;
    return if not $url or not $self->{ base_url };
    $_ = URI->new_abs( $url, $self->{ base_url } )->as_string(); 
    $url = $1 if( $url =~ /(.*)\/$/ );
    return $url;  
}


# arguments for the handlers : 
#   $tagname    => the name of the start tag, 
#   $attr       => hash-reference containing the attributes as key/value pairs, 
#   $attrseq    => an array-reference which lists the attribute keys in the order in which they appeared in the tag
#   $origtext   => the original text as it appeared in the HTML snippet
sub start{ 
    my ( $self, $tagname, $attr, $attrseq, $origtext ) = @_;
    
    if ($tagname eq 'a' and $attr->{ href } and not $attr->{ href } =~ /^((#)|(mailto)).*/ ) {
        my $url = $self->resolve( $attr->{ href } );
        #print "     URL found: $url\n";
        push @{ $self->{ links } }, $url unless not $url
    }
}

