#!/usr/bin/env perl




#----------------------------------------------------
#------------------- crawler ------------------------
package SimpleCrawler;

use warnings;
use strict;

use Data::Dumper;
use Carp;

use LWP::UserAgent;
use HTTP::Request;
use HTTP::Date qw( parse_date );
use HTML::Parser;
use URI;

use threads;
use Thread::Queue;
use threads::shared;

use Exporter;
use vars qw( @ISA @EXPORT $VERSION );

$VERSION = '1.00';
@ISA = qw(Exporter);
@EXPORT = qw( $COUNT $LOCS $STATUS $CONTENT_TYPE $LAST_MOD );

my $debug = '';
our( $COUNT, $LOCS, $STATUS, $CONTENT_TYPE, $LAST_MOD ) = ( 0 ... 4 );


#----------------------------------------------------


sub new{
    print "SimpleCrawler::new eaw args", Dumper(@_) if $debug;
    my $class = shift or croak('Usage : SimpleCrawler::new( $r_options )\n');
    my $user_options = shift;
    
    my $self = {
        mode => ( $user_options and $_ = $user_options->{ mode } and $_ =~ /^(domain)|(all)$/ ? $user_options->{ mode } : 'all' ),
        tags => ( $user_options and $_ = $user_options->{ tags } and join( ' ', $_ =~ /(\w+)/g ) or ' a src link img ' )
    };
    
    print "SimpleCrawler::new parsed args", Dumper( $self ) if $debug;
    return bless $self, $class;
}

#----------------------------------------------------

# crawls the website and returns a hash where the keys are the urls (from the same domain)
# found and the values are arrays : 0 => nbr of times it was encountered, 1 => content-type, 2 => last-modified ( or '' ) 
# options : link => full url to parse, html_only => (optional) returns only the html pages
sub gather_links{ # %links ( $self, $link )
    my $self = shift or croak( 'This is an instance method...' );
    my ( $base_link ) = shift;
    $base_link = $1 if $base_link =~ m|(.+)/$|;
    
    my ( $queue, %parsed, %found, $running, @active ) : shared; # to keep track of parsed pages
    my @threads; # threads
    my @reported_tags = qw( a img style javascript link );
    my $all = 0;
    
    # initialises the job queue
    $queue = Thread::Queue->new();
    $queue->enqueue( $base_link );
    
    # initialises the variables
    $running = 1;

    # loads the threads code 
    eval $self->__load_thread_code( $base_link ); print $@;
    print "Launching parsing process...\n";
    
    #launches the threads
    for (0 ...  3 ){
        $active[$_] = 1;
        push @threads, threads->create( \&code_thread, $_ );
        
    }
    
    # waits for the workers to finish
    while( grep /1/, @active ){ sleep 0.1; } # waits for the threads to terminate

    # joins the threads
    $running = 0; # stops the threads
    for( @threads ){
        my $r = $_->join();
    }

    
    print "parsing done...\n";
    
    # reshapes the results
    my %results;
    map { 
        $results{ $_ } = [ split( '#', $found{ $_ } ) ]; 
        $results{ $_ }->[$LOCS] = [ split( ',', $results{ $_ }->[$LOCS] ) ]; 
    } keys %found;
    
    print Dumper( \%results ), "\n found ", scalar( keys %results ), "\n" if $debug;

    return \%results;
}
  
#----------------------------------------------------

sub __load_thread_code{
    my ( $self, $base_url ) = @_;

    open F, "thread_code.txt" or die "What the heck did you do with my threads code ??\n";
    local $/ = undef;
    my $_ = <F>;
    close F; 
    
    s/BASE_LINK/$base_url/ge;
    s/TAGS/ $self->{ tags }  /ge;
    s/<FROM_DOMAIN_ONLY>(.+?)<\/FROM_DOMAIN_ONLY>/$1/smg if $self->{ mode } eq 'domain';
    s/<[A-Z_]+>(.+?)<\/[A-Z_]+>//smg;
    
    print $_ if $debug;
    return $_;
}

#-------------------------------------------------------
#-------------------- parser ---------------------------
package __LinkParser;

# simple parser, which : 
# finds all the <a> tags in a page, resolves their href using the base url provided
# and gathers the latter in self{ links }
# http://perlmeme.org/tutorials/html_parser.html
# http://search.cpan.org/dist/HTML-Parser/Parser.pm
use warnings;
use strict;
use Carp;


use Data::Dumper;

use base qw(HTML::Parser);

sub scheme{
    my $self = shift or croak( 'this is an instance method' );
    @_ ? $self->{ scheme } = shift : $self->{ scheme };
}

sub base_url{
    my $self = shift or croak( 'this is an instance method' );
    @_ ? $self->{ base_url } = shift : $self->{ base_url };
}

# returns the absolute url, using the url given in get_and_parse as a base
sub resolve{ # $absolute_url ( $self, $url )
    my ( $self, $url ) = @_;
    return if not $url or not $self->{ base_url };
    
    $url = URI->new_abs( $url, $self->{ base_url } );
    #return '' if $self->{ scheme } and not $url->scheme eq $self->{ scheme };
    
    $url = $url->as_string(); 
    $url = $1 if( $url =~ /(.*)\/$/ );
    
    return $url;  
}


1;
