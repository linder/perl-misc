sub code_thread{
    my $id = shift;
    # creates a useragent for the http requests
    my $ua = LWP::UserAgent->new;
    $ua->timeout( 30 );

    # creates and init the parser
    my $parser = __LinkParser->new();
    $parser->scheme( 'http' );
    $parser->report_tags( qw( TAGS ) ); # only reports these tags

    $parser->handler( start => # start tag handler
        sub{
            my ( $self, $tagname, $attr, $line ) = @_;
            
            #if ($tagname eq 'a' and $attr->{ href } and not $attr->{ href } ) {
                
                my $url = $attr->{ href } or $attr->{ src };
                return if not $url or $url =~ /^((#)|(mailto)).*/ ; # don't bother with mailto or relative anchor
                
                # resolves the link 
                $url = $self->resolve( $attr->{ href } );
                return if not $url or $url !~ /^http/ <FROM_DOMAIN_ONLY>or $url !~ m|BASE_LINK| </FROM_DOMAIN_ONLY>;
                
                # strips the anchor part + the args
                $url = $1 if $url =~ /^([^#].*)\/#.*/; 
                $url = $1 if $url =~ /^([^\?].*)\/\?.*/; 
                
                # if already encountered, increments its counter and return
                if( exists $found{ $url } ) { 
                    $found{ $url } =~ s/^([0-9]+)/int($1) + 1/e; 
                    $found{ $url } =~ s/^([^#]+#)([^#]+)(.*)/$1 . ( $2 ? "$2," : '' ) . $self->base_url . ":$line" . $3 /e; 
                    return; 
                }else{
                    # if never encountered before, add 1
                    $found{ $url } = "1#" . $self->base_url . ":$line"; 
                }                
                $queue->enqueue( $url ); # if $url =~ m|BASE_LINK|;
            }, 
            'self, tagname, attr, line' 
        #} 
    );

    # while the running flag is true
    while ( $running  ) {
        my $item = $queue->dequeue_nb();
        
        if( not $item ){ # no item, waits a moment
            print "  $id : waiting...\n" if $debug;
            $active[$id] = 0; # marks this thread as idle
            sleep 1;
            next;
        }

        $active[$id] = 1; # marks this thread as active
        
        # continue if the page has already been visited (or is null)
        next if not $item or $parsed{ $item };
        $parsed{ $item } = 1; # marks the page as visited
        
        # gets the page
        my $response = $ua->request( HTTP::Request->new(GET => $item ) );
        
        # stores the headers infos : content-type and last-mod, if any
        my $infos;
        
        $infos .= "#" . $response->status_line . "#";
        $infos .= ( $response->header( 'Content-Type' ) =~ m|^([^;]+)| and $1 ) . "#";
        if( $response->header( 'last-modified' ) ){
            $infos .= "$3-$2-$1" if 
                $_ = parse_date( $response->header( 'last-modified' ) ) and $_=~ m|([0-9]{4})-([0-9]{2})-([0-9]{2})|;
        }
        $found{ $item } = ( $found{ $item } or "1#base" ) . $infos;

        # don't parse if not an html doc from the same domain, content null or 404 not found
        next if $item !~ m|BASE_LINK| 
            or $response->header( 'Content-Type' ) !~ m|text/html| 
            or not $response->content 
            or $response->status_line =~ /404/;
            
        print "$id parsing : $item\n" if $debug;
        
        # re-inits and launches the parser
        $parser->base_url( $item );
        eval{ $parser->parse( $response->content ) };
        print "Error : $@\n" if $@; 
        
        print $id, " queue: ", $queue->pending(), "\n" if $debug;
    }
}

