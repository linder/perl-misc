#!/usr/bin/env perl

use warnings;
use strict;


use Getopt::Long;
use Data::Dumper;
use URI;
use DateTime;

use Cwd;
use File::Spec::Functions;

push @INC, ( Cwd::abs_path($0) =~ /(.*\/)[^\/]*/ and $1 );
BEGIN{
    require SimpleCrawler; SimpleCrawler->import();
}

#my $base_link = "http://www.perrin-traiteur.ch";
#my $base_link = "http://www.santeparlayurveda.ch/";
#my $base_link = "http://www.madeleine-traiteur.ch";
#my $base_link = "http://dytherapie.ch";
#my $base_link = "http://jmolivier.blog.tdg.ch/";

my $debug = 1;

my $mode;
$mode = splice @ARGV, 0, 1 and $mode =~ /(checker)|(sitemap)/ 
    or die "Usage LinkUtils.pl checker|sitemap\n";

checker() if( $mode eq 'checker' );
sitemap() if( $mode eq 'sitemap' );

#-------------------------------------------------------
#-------------------- checker --------------------------
sub checker{
    
    my %options = ( coverage => 'all', tags => '' );

    GetOptions(

        'u|url=s' => sub{
            my ( $opt_name, $opt_value ) = @_; 
            if( $_ = URI->new( $opt_value, 'http' ) and $_->scheme ){ $options{ url } = $opt_value; }
            else{ die "not a valid HTTP link...\n"; }
        },
        
        't|tags=s'      => sub{ $options{ tags } = $_[1] },
        
        'a|all'         => sub{ $options{ coverage } = 'all'; },
        
        'd|domain'      => sub{ $options{ coverage } = 'domain'; },

    );
    
    
    die "Url not given or not valid...\n" unless ( $options{ url } and $_ = URI->new( $options{ url } , 'http' ) and $_->scheme );

    print Dumper( \%options ) if $debug;

    my $crawler = SimpleCrawler->new( { mode => $options{ coverage }, tags => $options{ tags } } );
    my $rlinks = $crawler->gather_links( $options{ url } );
    print Dumper( $rlinks ) if $debug;
    
    my @suspicious_links = grep { $rlinks->{ $_ }[$STATUS] !~ /200/ } keys %$rlinks;
    
    print "Everything is fine, nothing suspicious has been found. Congrats !\n" 
        and exit(0) unless scalar( @suspicious_links );
    
    print "Found ", scalar( @suspicious_links ), " suspicious links: \n";
    foreach my $link ( @suspicious_links ){
        print " * $link (", $rlinks->{ $link }[$STATUS], ") : \n";
        map { print "   - ", $_ , "\n" } @{ $rlinks->{ $link }[$LOCS] };
        
    }
    
}

#-------------------------------------------------------
#-------------------- sitemap --------------------------
sub sitemap{
    
    my %options = ( 
        tags        => '', 
        accept   => 'html', 
        last_mod     => '',
        file        => '' 
    );

    GetOptions(

        'u|url=s'       => sub{ $options{ url } = $_[1] },
        
        't|tags=s'      => sub{ $options{ tags } = $_[1] },
        
        'a|accept=s'    => sub{ print $_[1]; $options{ accept } .= "|" . join( '|', $_[1] =~ /(\w+)/g ); },

        'f|file=s'      => sub{ 
            $_ = $_[1];
            s/(~)/${ENV{HOME}}/g;
            $_ = Cwd::realpath( $_ ); 
            die "The given filepath is invalid...\n" unless $_;
            $_ = catfile( $_, 'sitemap.xml' ) if -d $_;
            $options{ file } = $_;
        },
        
        'excluded=s'    => sub{
            my ( $opt_name, $opt_value ) = @_; 
            @_ = split( ',', $opt_value );
            $options{ excluded } = \@_ unless not scalar( @_ ); 
        },
        
        'l|lastmod=s'     => sub{ 
            my ( $opt_name, $opt_value ) = @_; 
            $options{ last_mod } = DateTime->now( time_zone=>'local' )->ymd('-') and return 
                if $opt_value eq 'auto';
            
            die "Error: lastmod options takes a date in the following format : YYYY-MM-dd\n"
                if $opt_value !~ /^20[0-9]{2}-((0[0-9])|(1[0-2]))-(([0-2][0-9])|(3[0-1]))$/;
            
            $options{ last_mod } = $opt_value; 
        }
    );
    
    die "Url not given or not valid...\n" unless $options{ url } and $_ = URI->new( $options{ url } , 'http' ) and $_->scheme;
    
    print Dumper \%options if $debug;

    my $rlinks = SimpleCrawler->new( { mode => 'domain', tags => $options{ tags } } )->gather_links( $options{ url } );
    my $regex = '\.(' . join( ')|(', $options{ excluded }  ) . ')' if $options{ excluded };
    
    my $sitemap = '
<?xml version="1.0" encoding="UTF-8"?>
<urlset
      xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9
            http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">';
    
            
    foreach my $link ( sort keys %$rlinks ){        
        next if $rlinks->{ $link }->[$CONTENT_TYPE] !~ $options{ accept };
        next if defined $regex and $link =~ /$regex/i;
        
        if( $rlinks->{ $link }->[$STATUS] !~ /200/ ){
            print "Detected potentially broken link\n   $link\nreferenced at \n";
            map { print "   - ", $_ , "\n" } @{ $rlinks->{ $link }[$LOCS] };
            print "skipping it...\n";
            next;
        }
        
        $sitemap .= "
        <url>
          <loc>" . $link . "</loc>"
            . ( $options{ last_mod } ? "
          <lastmod>" . ( $rlinks->{ $link }->[$LAST_MOD] or $options{ last_mod } ) . "</lastmod> " : "" ) . 
        "
        </url>
";
    }
    
    $sitemap .= "</urlset>
";
    
    if( $options{ file } ){
        open F, '>' . $options{ file } or die "Could not open " . $options{ file } . "\n";
        print F $sitemap;
        close F;
    }else{
        print $sitemap;
    }
    
    print "\nSitemap generated.\n";
    
}


