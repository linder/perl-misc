sub test_thread{
    my $id = shift;
    # creates a useragent for the http requests
    my $ua = LWP::UserAgent->new;
    $ua->timeout( 30 );

    # creates and init the parser
    my $parser = LinkParser->new();
    $parser->report_tags( qw( a ) ); # only reports a tag

    $parser->handler( start => # start tag handler
        sub{
            my ( $self, $tagname, $attr, $attrseq, $origtext ) = @_;
            
            if ($tagname eq 'a' and $attr->{ href } and not $attr->{ href } =~ /^((#)|(mailto)).*/ ) {

                # resolves the link 
                my $url = $self->resolve( $attr->{ href } );
                
                return if not $url 
                <HTML_ONLY># returns if we want only html and it has an extension
                or $url =~ /\.((jpg)|(png)|(css)|(js)|(pdf)|(xml)|(gif))$/i
                </HTML_ONLY>;
                
                # strips the anchor part
                $url = $1 if $url =~ /^([^#].*)#.*/; 
                
                # if already encountered, increments its counter and return
                if( exists $found{ $url } ) { 
                    $found{ $url } =~ s/^([0-9]+)/int($1) + 1/e; 
                    return; 
                }else{
                    # if never encountered before, add 1
                    $found{ $url } = "1"; 
                }
                
                $queue->enqueue( $url ) if $url =~ /$base_link/;
            } 
        } 
    );

    # while the running flag is true
    while ( $running  ) {
        my $item = $queue->dequeue_nb();
        
        if( not $item ){ # no item, waits a moment
            print "  $id : waiting...\n";
            $active[$id] = 0; # marks this thread as idle
            sleep 1;
            next;
        }

        $active[$id] = 1; # marks this thread as active
        
        # continue if the page has already been visited (or is null)
        next if not $item or $parsed{ $item };
        $parsed{ $item } = 1; # marks the page as visited
        
        # gets the page
        my $response = $ua->request( HTTP::Request->new(GET => $item ) );
        
        <HTML_ONLY>
        next if $response->header( 'Content-Type' ) !~ /text\/html/;
        </HTML_ONLY>
        # stores the headers infos : content-type and last-mod, if any
        <DETAILS>
        my $infos;
        $infos = "1" if not $found{ $item };
        $infos .= "#" . ( $response->header( 'Content-Type' ) =~ /^([^;]+)/ and $1 ) . "#";
        $infos .= "$3-$2-$1" if $response->header( 'last-modified' )
            and parse_date( $response->header( 'last-modified' ) ) =~ /([0-9]{4})-([0-9]{2})-([0-9]{2})/;
        
        $found{ $item } .= $infos;
        </DETAILS>
        # don't parse if not an html doc
        next unless $response->header( 'Content-Type' ) =~ /text\/html/;
        print "$id parsing : $item\n";
        
        # re-inits and launches the parser
        $parser->base_url( $item );
        eval{ $parser->parse( $response->content ) };
        print "Error : $@\n" if $@; 
        
        print $id, " queue: ", $queue->pending(), "\n";
    }
}

